const axios = require('axios');
const cheerio = require('cheerio');
const CronJob = require('cron').CronJob;
const nodemailer = require('nodemailer');
const readline = require('readline');
const url = require('./config');
require('dotenv').config();

const baseURL = 'https://www.tori.fi/uusimaa';
// const fullURL = baseURL + url.desktops;
const fullURL = '';

const promptUser = () => {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  rl.question('Enter your region: ', function (region) {
    fullURL = `http://localhost:8888/${region}.html`;
    rl.close();
  });

  rl.on('close', function () { 
    console.log('\nThank you');
    process.exit(0);
  });
};

const getItems = async () => {
  return axios
    .get(fullURL)
    .then(response => {
      const desktops = [];
      const priceLimit = 600;
      const $ = cheerio.load(response.data);

      $('.list_mode_thumb a').each(function (i, element) {
        const $row = $(element);
        const $title = $row.find('.li-title');
        const $price = $row.find('.list_price.ineuros');
        const $link = $row.attr('href');

        const priceNumber = $price.text().match(/\d+/) * 1;

        if (priceNumber <= priceLimit) {
          const desktop = {
            title: $title.text(),
            price: $price.text(),
            link: $link,
          };
          desktops.push(desktop);
        }
      });
      return desktops;
    })
    .catch(error => {
      console.log(error);
    });
};

const getDeals = async (prevData, newData) => {
  const newItems = [];
  itemFound = false;

  if (JSON.stringify(await prevData) === JSON.stringify(await newData)) {
    // console.log('No new desktops!');
    return false;
  }

  for (let i = 0; i < newData.length; i++) {
    for (let j = 0; j < prevData.length; j++) {
      if (JSON.stringify(newData[i]) === JSON.stringify(prevData[j])) {
        itemFound = true;
        break;
      }
    }
    if (itemFound !== true) {
      newItems.push(newData[i]);
    }
  }
  return newItems;
};
const messageBody = body => {
  let htmlBody = '<h2>Check those deals: </h2><ol>';

  body.forEach(item => {
    htmlBody +=
      '<li>' +
      item.title +
      'Price: ' +
      item.price +
      ' Link: ' +
      item.link +
      '</li>';
  });
  htmlBody += '</ol>';

  return htmlBody;
};

const sendEmail = message => {
  const transport = nodemailer.createTransport({
    service: 'Hotmail',
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS,
    },
  });

  const mailOptions = {
    from: process.env.EMAIL_USER,
    to: process.env.EMAIL_RECIPIENT,
    subject: `New deals found`,
    html: messageBody(message),
  };

  transport.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log('Error while sending mail: ' + error);
    } else {
      console.log('Message sent: %s', info.messageId);
    }
    transport.close();
  });
};

const monitor = async () => {
  let prevItems = await getItems();

  const job = new CronJob(
    // '0 */15 * * * *',
    '*/20 * * * * *',
    async function () {
      // console.log('Cron again');
      const newItems = await getItems();
      const newDeals = await getDeals(prevItems, newItems);
      // sendEmail(prevItems);
      if (newDeals !== false) {
        prevItems = [...newItems];
        sendEmail(newDeals);
      }
      // console.log('new Deals:', newDeals);
      // console.log('prevItems', prevItems);
      // console.log('new Items:', newItems);
    },
    null,
    true,
    null,
    null,
    true
  );
  job.start();
};

monitor();
