/* eslint-disable arrow-parens */
const express = require('express');
const scraper = require('./scraper');
const port = 3002;

const app = express();

app.get('/', (req, res) => {
  scraper.getItems().then(data => res.json(data));
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
