const url = {
  cases:
    '/tietokoneet_ja_lisalaitteet/komponentit?ca=18&cg=5030&c=5038&w=1&st=s&st=k&st=u&st=h&st=g&com=case',
  desktops:
    '?q=&cg=5030&w=1&st=s&st=k&st=u&st=h&st=g&c=5034&ps=&pe=&ca=18&l=0&md=th',
  fans:
    '/tietokoneet_ja_lisalaitteet/komponentit?ca=18&cg=5030&c=5038&w=1&st=s&st=k&st=u&st=h&st=g&com=processor',
  graphic_cards:
    '/tietokoneet_ja_lisalaitteet/komponentit?ca=18&cg=5030&c=5038&w=1&st=s&st=k&st=u&st=h&st=g&com=graphic_card',
  hardisks:
    '/tietokoneet_ja_lisalaitteet/komponentit?ca=18&cg=5030&c=5038&w=1&st=s&st=k&st=u&st=h&st=g&com=harddisk',
  laptops:
    '?q=&cg=5030&w=1&st=s&st=k&st=u&st=h&st=g&c=5032&ps=&pe=&ca=18&l=0&md=th',
  memories:
    '/tietokoneet_ja_lisalaitteet/komponentit?ca=18&cg=5030&c=5038&w=1&st=s&st=k&st=u&st=h&st=g&com=memory',
  motherboards:
    '?q=&cg=5030&w=1&st=s&st=k&st=u&st=h&st=g&c=5038&ps=&pe=&ca=18&l=0&md=th',
  processors:
    '/tietokoneet_ja_lisalaitteet/komponentit?ca=18&cg=5030&c=5038&w=1&st=s&st=k&st=u&st=h&st=g&com=processor',
  other_components:
    '/tietokoneet_ja_lisalaitteet/komponentit?ca=18&cg=5030&c=5038&w=1&st=s&st=k&st=u&st=h&st=g&com=processor',
  tablets:
    '?q=&cg=5030&w=1&st=s&st=k&st=u&st=h&st=g&c=5031&ps=&pe=&ca=18&l=0&md=th',
};

module.exports = url;
